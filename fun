import telebot
import const
import btn
import fun_db
import datetime
import re
import xlwt


def create_report():
    # стили, шрифты
    font0 = xlwt.Font()
    font0.name = 'Times New Roman'
    font0.height = 250
    style0 = xlwt.XFStyle()
    style0.font = font0
    style1 = xlwt.XFStyle()
    style1.font = font0
    style1.num_format_str = 'DD-MM-YY'

    # создание объекта
    wb = xlwt.Workbook()

    # название страницы
    ws = wb.add_sheet('Orders')

    # настройка ширины колонок
    ws.col(0).width = 3700
    ws.col(1).width = 3700
    ws.col(2).width = 9000
    ws.col(3).width = 4500
    ws.col(4).width = 8000
    ws.col(4).hight = 4000
    ws.col(5).width = 2000
    ws.col(6).width = 5000
    ws.col(7).width = 4000
    ws.col(8).width = 4000

    # шапка
    ws.write(0, 0, 'Номер заказа', style0)
    ws.write(0, 1, 'Номер чата', style0)
    ws.write(0, 2, 'ФИО', style0)
    ws.write(0, 3, 'Номер телефона', style0)
    ws.write(0, 4, 'Текст заказа', style0)
    ws.write(0, 5, 'Сумма заказа', style0)
    ws.write(0, 6, 'Дата заказа', style0)
    ws.write(0, 7, 'Статус оплаты', style0)
    ws.write(0, 8, 'Способ связи', style0)

    # чтение таблицы "Заказы"
    all_orders = fun_db.read_all_orders()

    # запись данных о заказах в таблицу
    i = 1
    for order in all_orders:
        ws.write(i, 0, str(order[0]), style0)
        ws.write(i, 1, str(order[1]), style0)
        ws.write(i, 2, str(order[2]), style0)
        ws.write(i, 3, str(order[3]), style0)
        ws.write(i, 4, str(order[4]), style0)
        ws.write(i, 5, int(order[5]), style0)
        ws.write(i, 6, str(order[6]), style0)
        ws.write(i, 7, str(order[7]), style0)
        ws.write(i, 8, str(order[8]), style0)
        i = i+1

    # подвал
    ws.write(i, 0, 'Дата отчета ', style0)
    ws.write(i, 1, datetime.datetime.now(), style1)
    ws.write(i, 4, 'Сумма всех заказов', style0)
    ws.write(i, 5, xlwt.Formula('sum(F2:F%s)' %i), style0)
    ws.write(i+1, 4, 'Количество заказов', style0)
    ws.write(i+1, 5, i-1, style0)

    # перезапись/сохранение отчета
    wb.save('OrdersReport.xls')


def creation_of_basket(id_of_chat):
    all_product_of_chat = fun_db.all_product_of_chat(id_of_chat)
    text_order = 'В вашей корзине \n'
    sum = 0
    for product_basket in all_product_of_chat:
        for product_catalog in btn.catalog:
            if product_catalog[0] == product_basket[1]:
                sum += product_basket[4] * product_basket[2]
                text_order += str(product_catalog[1]) + '    ' + str(product_basket[4]) + '  шт. по ' + str(
                    product_basket[2]) + ' руб. в сумме на ' + str(product_basket[4] * product_basket[2]) +' руб.\n'
    text_order += 'Итого к оплате ' + str(sum) + ' руб.'
    return [text_order, sum]


def last_selected_product(name_of_last_product):
    for product in btn.catalog:
        if name_of_last_product == product[1]:
            return product


def get_names_of_products():
    product_in_db = []
    i = len(vars(btn.markup_catalog).get('keyboard'))
    while i > 1:
        i -= 1
        product_in_db.append(vars(btn.markup_catalog).get('keyboard')[i][0].get('text'))
    return product_in_db


def convert_data(all_id_of_orders_):
    all_id_of_orders = []
    for order in all_id_of_orders_:
        all_id_of_orders.append(order[0])
    return all_id_of_orders